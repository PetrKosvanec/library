import java.util.ArrayList;

public class Library {
	
	String address;
	ArrayList<Book> listOfBooks = new ArrayList<Book>();
	
	public Library(String libraryAddress) {
		address = libraryAddress;
	}

	public void addBook(Book book) {
		book.returned();
		listOfBooks.add(book);

		System.out.println(book.getTitle());
		System.out.println("borrowed: " + book.isBorrowed());

		int size = listOfBooks.size();
		System.out.println(size);
	}

	private static void printOpeningHours() {
		System.out.println("Libraries are open daily from 9am to 5pm.");
	}

	private void printAddress() {
		System.out.println(this.address);
	}

	private void findBookAndBorrowOrReturn(String strWantedTitle, boolean toBorrowTRUEtoReturnFALSE) {
		if (listOfBooks.size() > 0) {

			for (Book book : listOfBooks) {
				if (strWantedTitle.equals(book.getTitle())) {
					if (book.isBorrowed() && toBorrowTRUEtoReturnFALSE == false) {
						book.returned();	// -> return the book
						System.out.println("You successfully returned " + book.getTitle());
					}
					else if (!(book.isBorrowed()) && toBorrowTRUEtoReturnFALSE == false) {
						System.out.println("Sorry, this book is not borrowed yet.");
					}
					else if (!(book.isBorrowed()) && toBorrowTRUEtoReturnFALSE == true) {
						book.borrowed();	// -> borrow the book
						System.out.println("You successfully borrowed " + book.getTitle());
					}
					else if (book.isBorrowed() && toBorrowTRUEtoReturnFALSE == true) {
						System.out.println("Sorry, this book is already borrowed.");
					}
				}
			}
		}
		else if (listOfBooks.size() == 0) {
			System.out.println("Sorry, this book is not in our catalog.");
		}
	}

	private void borrowBook(String strBookTitle) {
		findBookAndBorrowOrReturn(strBookTitle, true);
	}

	private void printAvailableBooks() {
		if (this.listOfBooks.size() == 0) {
			System.out.println("No book in catalog");
		}
		else {
			for (int i = 0; i < listOfBooks.size(); i++) {
				System.out.println(listOfBooks.get(i).getTitle());
			}
		}
	}

	private void returnBook(String strBookTitle) {
		findBookAndBorrowOrReturn(strBookTitle, false);
	}

    public static void main(String[] args) {
        // Create two libraries
        Library firstLibrary = new Library("10 Main St.");
        Library secondLibrary = new Library("228 Liberty St.");

        // Add four books to the first library
        firstLibrary.addBook(new Book("The Da Vinci Code"));
        firstLibrary.addBook(new Book("Le Petit Prince"));
        firstLibrary.addBook(new Book("A Tale of Two Cities"));
        firstLibrary.addBook(new Book("The Lord of the Rings"));

        // Print opening hours and the addresses
        System.out.println("Library hours:");
        printOpeningHours();
        System.out.println();

        System.out.println("Library addresses:");
		firstLibrary.printAddress(); // "10 Main St."
        secondLibrary.printAddress(); // "228 Liberty St."
        System.out.println();

        // Try to borrow The Lords of the Rings from both libraries
        System.out.println("* Borrowing The Lord of the Rings (from first library):");
        firstLibrary.borrowBook("The Lord of the Rings");

	    System.out.println("* Borrowing The Lord of the Rings (from first library again):");	
        firstLibrary.borrowBook("The Lord of the Rings");

		System.out.println("* Borrowing The Lord of the Rings (from second library):");
        secondLibrary.borrowBook("The Lord of the Rings");
        System.out.println();

        // Print the titles of all available books from both libraries
        System.out.println("Books available in the first library:");
        firstLibrary.printAvailableBooks();
        System.out.println();
        System.out.println("Books available in the second library:");
        secondLibrary.printAvailableBooks();
        System.out.println();

        // Return The Lords of the Rings to the first library
        System.out.println("Returning The Lord of the Rings:");
        firstLibrary.returnBook("The Lord of the Rings");
        System.out.println();

        // Print the titles of available from the first library
        System.out.println("Books available in the first library:");
        firstLibrary.printAvailableBooks();
    }
} 
